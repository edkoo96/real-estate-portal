@extends('public.layouts.app2')

@section('title', 'Pridať nový inzerát')

@section('content')


    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&libraries=places" type="text/javascript"></script>
    <style>

        #map-canvas{
            width: 100%;
            height: 400px;
        }

    </style>

    <!-- Breadcrumb -->
    <div class="site-breadcrumb">
        <div class="container">
            <a href="index"><i class="fa fa-home"></i>Domov</a>
            <span><i class="fa fa-angle-right"></i>@yield('title')</span>
        </div>
    </div>
    <!-- Breadcrumb end -->


    <!-- page -->
    <section class="page-section">
        <div class="container">
            <div class="row">

                    <div class="col-lg-12">
                        <div class="container">
                            <div class="section-title">
                                <h3>Vytvorenie inzerátu</h3>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                            <form method="post" href="{{action("EstateController@addUserEstate")}}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h5>Osobné údaje</h5>
                                        <p><br>Meno: *</p>
                                        <input type="text" placeholder="" class="form-control" name="name">
                                        <p>Priezvisko: *</p>
                                        <input type="text" placeholder="" class="form-control" name="surname">
                                        <p>E-mail: *</p>
                                        <input type="text" placeholder="" class="form-control" name="email">
                                        <p>Telefónne číslo:</p>
                                        <input type="text" placeholder="" class="form-control" name="telephone">
                                    </div>

                                    <div class="col-lg-6">
                                        <h5>Popis a vlastnosti nehnuteľnosti:</h5>
                                        <p><br>Titulný text:</p>
                                        <input type="text" placeholder="" class="form-control" name="estate_name">
                                        <p><br>Popíšte slovne nehnuteľnosť:</p>
                                        <textarea rows="10" placeholder="" class="form-control" id="description" name="description"></textarea>
                                        <script>
                                            CKEDITOR.replace( 'description' );
                                        </script>
                                        <p>Druh nehnuteľnosti: *</p>
                                        <select id="type" name="type" class="form-control">
                                            @foreach($estate_types as $estate_type)
                                                <option value="{{$estate_type->id}}">{{$estate_type->name}}</option>
                                            @endforeach
                                        </select>
                                        <p><br>Typ: *</p>
                                        <select class="form-control" name="type_offer">
                                            <option value="sale">Predaj</option>
                                            <option value="rent">Prenájom</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-12">

                                        <h5>Inzerát</h5>
                                        <p><br>Adresa lokality: *</p>
                                        <p>Mesto: *</p>
                                        <input type="text" class="form-control" name="searchmap" id="searchmap" placeholder="">
                                        <p>Mapa: </p>
                                        <div id="map-canvas"></div>
                                        <p>Lat</p>
                                        <input type="text" class="form-control" name="lat" id="lat" placeholder="">
                                        <p>Lng</p>
                                        <input type="text" class="form-control" name="lng" id="lng" placeholder="">

                                        <br><br>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <p>Cena v €:</p>
                                                <input type="number" placeholder="" class="form-control" name="prize">
                                                <p>Rozloha v m²:</p>
                                                <input type="number" placeholder="" class="form-control" name="space">
                                                <p>Počet spální:</p>
                                                <input type="number" placeholder="" class="form-control" name="bedrooms">
                                            </div>
                                            <div class="col-lg-6">
                                                <p>Počet kúpelní:</p>
                                                <input type="number" placeholder="" class="form-control" name="bathrooms">
                                                <p>Počet garáží:</p>
                                                <input type="number" placeholder="" class="form-control" name="garages">
                                                <p>Počet parkovacích miest:</p>
                                                <input type="number" placeholder="" class="form-control" name="parking">
                                            </div>
                                        </div>

                                        <p><br>Nahrať fotografie:</p>

                                        <input type="file" name="photos[]" id="photos" placeholder="Pridať fotografie" multiple />

                                        <p> * povinné údaje</p>
                                        <button class="site-second-btn">ODOSLAŤ</button>

                                    </div>

                                </div>
                            </form>

                        </div>
                    </div>

            </div>
        </div>
    </section>
    <!-- page end -->

    <script type="text/javascript">

        $(document).ready(function() {

            $(".btn-success").click(function(){
                var html = $(".clone").html();
                $(".increment").after(html);
            });

            $("body").on("click",".btn-danger",function(){
                $(this).parents(".control-group").remove();
            });

        });

    </script>

    <script>

        var map = new google.maps.Map(document.getElementById('map-canvas'),{
            center:{
                lat: 48.71,
                lng: 18.84
            },
            zoom:9
        })

        var marker = new google.maps.Marker({
            position: {
                lat: 48.71,
                lng: 18.84
            },
            map: map,
            draggable: true
        });

        var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

        google.maps.event.addListener(searchBox, 'places_changed', function () {

            var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, places;

            for(i=0; place=places[i]; i++){
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location);
            }

            map.fitBounds(bounds);
            map.setZoom(15);

        });

        google.maps.event.addListener(marker, 'position_changed', function () {

            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#lat').val(lat);
            $('#lng').val(lng);
        })

    </script>

@endsection