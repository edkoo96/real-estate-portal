@extends('public.layouts.app2')

@section('title', 'Blog')

@section('content')

    <!-- Breadcrumb -->
    <div class="site-breadcrumb">
        <div class="container">
            <a href="index"><i class="fa fa-home"></i>Domov</a>
            <span><i class="fa fa-angle-right"></i>@yield ('title') </span>
        </div>
    </div>
    <!-- Breadcrumb end -->


    <!-- page -->
    <section class="page-section blog-page">
        <div class="container">
            <div class="row">
                @foreach($rss_items as $rss_item)
                    <div class="col-lg-4 col-md-6 blog-item">
                        <img src="{{$rss_item['img']}}" alt="">
                        <h5><a href="{{$rss_item['link']}}">{{$rss_item['title']}}</a></h5>
                        <div class="blog-meta">
                            <span><i class="fa fa-user"></i>{{$rss_item['author']}}</span>
                            <span><i class="fa fa-clock-o"></i>{{$rss_item['pubDate']}}</span>
                        </div>
                        <p>{{$rss_item['desc']}}</p>
                    </div>
            @endforeach
        </div>
    </section>
    <!-- page end -->

@endsection