<!DOCTYPE html>
<html>

<head>
    @include('public.includes.head')

        <section class="page-top-section set-bg" data-setbg="{{url('assets_public/img/page-top-bg.jpg')}}">
            <div class="container text-white">
                <h2>@yield('title')</h2>
            </div>
        </section>

</head>

<body>
@yield('content')

@include('public.includes.footer')
@include('public.includes.scripts')
</body>
</html>
