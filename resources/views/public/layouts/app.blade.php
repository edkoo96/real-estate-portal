<!DOCTYPE html>
<html>

<head>
    @include('public.includes.head')
</head>

<body>
@yield('content')

@include('public.includes.footer')
@include('public.includes.scripts')

</body>
</html>
