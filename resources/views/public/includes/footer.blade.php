
<!-- Footer section -->
<footer class="footer-section set-bg bg-dark" data-setbg="{{ url('assets_public/img/footer-bg.jpg') }}">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 footer-widget">
                <img src="{{ url('assets_public/img/logo.png') }}" alt="">
                <p>Na stránkach Real Estate Laboratory Vám ponúkame všetky realitné inzercie pre bežných užívateľov a realitné kancelárie.</p>
                <div class="contact-form">
                    <p><i class="fa fa-phone"></i> +421 901 121 121</p>
                    <p><i class="fa fa-envelope"></i> info@realestatelaboratory.com</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 footer-widget">
                <div class="double-menu-widget">
                    <h5 class="fw-title">POPULÁRNE REGIÓNY</h5>
                    <ul>
                        <li><a href="{{url('cities/Banska Bystrica')}}">Banská Bystrica</a></li>
                        <li><a href="{{url('cities/Bratislava')}}">Bratislava</a></li>
                        <li><a href="{{url('cities/Kosice')}}">Košice</a></li>
                        <li><a href="{{url('cities/Martin')}}">Martin</a></li>
                        <li><a href="{{url('cities/Nitra')}}">Nitra</a></li>
                        <li><a href="{{url('cities/Poprad')}}">Poprad</a></li>
                    </ul>
                    <ul>
                        <li><a href="{{url('cities/Presov')}}">Prešov</a></li>
                        <li><a href="{{url('cities/Prievidza')}}">Prievidza</a></li>
                        <li><a href="{{url('cities/Ruzomberok')}}">Ružomberok</a></li>
                        <li><a href="{{url('cities/Trencin')}}">Trenčín</a></li>
                        <li><a href="{{url('cities/Trnava')}}">Trnava</a></li>
                        <li><a href="{{url('cities/Zilina')}}">Žilina</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-6  footer-widget">
                <div class="newslatter-widget">
                    <h5 class="fw-title">NEWSLETTER</h5>
                    <p>Prajete si dostávať od nás novinky? <br>Zadajte svoj e-mail a my Vás budeme pravideľne informovať o správach vo svete nehnuteľností.</p>
                    <form class="footer-newslatter-form">
                        <input type="text" placeholder="Váš e-mail">
                        <button><i class="fa fa-send"></i></button>
                    </form>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="copyright">
                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>  | Edited
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer section end -->