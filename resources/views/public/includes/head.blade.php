<title>Real Estate Laboratory - @yield('title')</title>
<meta charset="UTF-8">
<meta name="description" content="Real Estate Laboratory">
<meta name="keywords" content="Real Estate Laboratory">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Favicon -->
<link href="{{url('assets_public/img/ikona.png')}}" rel="shortcut icon"/> 														<!-- !!! -->

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">

<!-- Stylesheets -->
<link rel="stylesheet" href="{{url('assets_public/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{url('assets_public/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{url('assets_public/css/animate.css') }}">
<link rel="stylesheet" href="{{url('assets_public/css/owl.carousel.css') }}">
<link rel="stylesheet" href="{{url('assets_public/css/style.css') }}">

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


<!-- Page Preloder-->
<div id="preloder">
    <div class="loader"></div>
</div>

<!-- Header section -->
<header class="header-section">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 header-top-left">
                    <div class="top-info">
                        <i class="fa fa-phone"></i>
                        +421 901 121 121
                    </div>
                    <div class="top-info">
                        <i class="fa fa-envelope"></i>
                        info@estatelab.eu
                    </div>
                </div>
                <div class="col-lg-6 text-lg-right header-top-right">
                    <div class="user-panel">
                        @if(Auth::guest())
                            <a href="{{url('/register')}}"><i class="fa fa-user-circle-o"></i> Registrácia</a>
                            <a href="{{url('/admin/login')}}"><i class="fa fa-sign-in"></i> Prihlásiť sa</a>
                        @else
                            <li>
                                <a href="{{url('/admin/')}}"><i class="fa fa-user-circle-o"></i> Administrácia</a>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="lnr lnr-exit"></i> <span>Odhlásiť sa</span></a>

                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="site-navbar">
                    <a href="/index" class="site-logo"><img src="{{url('assets_public/img/logo.png')}}" alt=""></a> 					<!-- !!! -->
                    <div class="nav-switch">
                        <i class="fa fa-bars"></i>
                    </div>
                    <ul class="main-menu">
                        <li><a href="{{url('index')}}">Domov</a></li>
                        <li><a href="{{url('categories')}}">Ponuka</a></li>
                        <li><a href="{{url('about')}}">O nás</a></li>
                        <li><a href="{{url('blog')}}">Blog</a></li>
                        <li><a href="{{url('contact')}}">Kontakt</a></li>
                        <li><a href="{{url('add')}}" class="site-btn">Pridať inzerát</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header section end -->