@extends('public.layouts.app2')

@section('title', 'Makléri')

@section('content')

    <div class="container">
        <p> <br><br> </p>
    </div>

    <!-- Breadcrumb -->
    <div class="site-breadcrumb">
        <div class="container">
            <a href="index"><i class="fa fa-home"></i>Domov</a>
            <span><i class="fa fa-angle-right"></i>@yield('Maklér')</span>
        </div>
    </div>
    <!-- Breadcrumb end -->

    <!-- page -->
    <section class="page-section categories-page">
        <div class="container">
            <div class="feature-item">
                @foreach($data_agents as $data_agent)
                    <div class="author-card">
                        <div class="author-img set-bg"><img src="{{ url( $data_agent->avatar_path ) }}" width="100" class="img-thumbnail" alt="Avatar"></div>
                        <div class="author-info">
                            <h5>{{$data_agent->name}} {{$data_agent->surname}}</h5>
                            <a href="{{url('profile/' . $data_agent->reality_offices->name)}}">{{$data_agent->reality_offices->name}}</a>
                        </div>
                        <div class="author-contact">
                            <p><i class="fa fa-phone"></i>{{$data_agent->telephone}}</p>
                            <p><i class="fa fa-envelope"></i>{{$data_agent->email}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                @forelse($data_details as $estate_detail)
                <!-- feature -->
                <div class="col-lg-4 col-md-6">
                    <div class="feature-item">
                        <div class="feature-pic set-bg" data-setbg="{{url($estate_detail->photos->first()['photo_path'])}}">
                            <div>
                                <div class="{{$estate_detail->estate_type_offer}}-notic">{{$estate_detail->estate_type_offer}}</div>
                            </div>
                        </div>
                        <div class="feature-text">
                            <div class="text-center feature-title">
                                <h3>{{$estate_detail->estate_name}}</h3>
                                <p><i class="fa fa-map-marker"></i> {{$estate_detail->address}}</p>
                            </div>
                            <div class="room-info-warp">
                                <div class="room-info">
                                    <div class="rf-right">
                                        <p><i class="fa fa-clock-o">{{date('d.m', strtotime($estate_detail->created_at))}}</i></p>
                                    </div>
                                </div>
                            </div>
                            <a href="{{url('single_list/'. $estate_detail->id)}}" class="room-price">{{$estate_detail->prize}} €{{ $estate_detail->estate_type_offer == 'rent' ? ' /mesiac' : '' }}</a>
                        </div>
                    </div>
                </div>
                @empty
                        <p><img src="{{url('assets_public/img/empty.jpg')}}"></p>
                 @endforelse
            </div>
        </div>
    </section>
    <!-- page end -->

@endsection