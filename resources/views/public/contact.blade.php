@extends('public.layouts.app2')

@section('title', 'Kontakt')

@section('content')


    <!-- Breadcrumb -->
    <div class="site-breadcrumb">
        <div class="container">
            <a href="index"><i class="fa fa-home"></i>Domov</a>
            <span><i class="fa fa-angle-right"></i>@yield ('title') </span>
        </div>
    </div>
    <!-- Breadcrumb end -->


    <!-- page -->
    <section class="page-section blog-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <img src="assets_public/img/contact.jpg" alt="">
                </div>
                <div class="col-lg-6">
                    <div class="contact-right">

                        <div class="section-title">
                            <h3>Kontaktujte nás</h3>
                        </div>
                        <form class="contact-form">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" placeholder="Meno a Priezvisko*">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" placeholder="E-mail*">
                                </div>
                                <div class="col-md-12">
                                    <textarea  placeholder="Vaša správa*"></textarea>
                                    <p>* povinné údaje</p>
                                    <button class="site-second-btn">ODOSLAŤ</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page end -->

@endsection