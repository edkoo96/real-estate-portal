@extends('public.layouts.app2')

@section('title', 'Hladanie')

@section('content')

    <div class="container">
        <p> <br><br> </p>
    </div>

    <!-- Breadcrumb -->
    <div class="site-breadcrumb">
        <div class="container">
            <a href="index"><i class="fa fa-home"></i>Domov</a>
            <span><i class="fa fa-angle-right"></i>@yield('title')</span>
        </div>
    </div>
    <!-- Breadcrumb end -->

    <!-- page -->
    <section class="page-section categories-page">
        <div class="container">
            @if(isset($details))
                <p><i class="fa fa-search"></i>Hľadaný výraz: {{$query}}</p>
            <div class="row">
                @forelse($details as $estate_detail)
                <!-- feature -->
                <div class="col-lg-4 col-md-6">
                    <div class="feature-item">
                        <div class="feature-pic set-bg" data-setbg="{{url($estate_detail->photos->first()['photo_path'])}}">
                            <div>
                                <div class="rent-notic">{{$estate_detail->estate_type_offer}}</div>
                            </div>
                        </div>
                        <div class="feature-text">
                            <div class="text-center feature-title">
                                <h3>{{$estate_detail->estate_name}}</h3>
                                <p><i class="fa fa-map-marker"></i> {{$estate_detail->address}}</p>
                            </div>
                            <div class="room-info-warp">
                                <div class="room-info">
                                    <div class="rf-left">
                                        <p><i class="fa fa-user">{{$estate_detail->users->name}} {{$estate_detail->users->surname}}</i></p>
                                    </div>
                                    <div class="rf-right">
                                        <p><i class="fa fa-clock-o">{{date('d.m', strtotime($estate_detail->created_at))}}</i></p>
                                    </div>
                                </div>
                            </div>
                            <a href="{{url('single_list/'. $estate_detail->id)}}" class="room-price">{{$estate_detail->prize}} €</a>
                        </div>
                    </div>
                </div>
                @empty
                        <p><img src="{{url('assets_public/img/empty.jpg')}}"></p>
                 @endforelse

            </div>
            <div class="site-pagination">
                <span>1</span>
                <a href="#">2</a>
                <a href="#">3</a>
                <a href="#"><i class="fa fa-angle-right"></i></a>
            </div>
            @elseif(isset($message))
                <h4><i class="fa fa-search"></i> {{$message}}</h4>
            @endif
        </div>
    </section>
    <!-- page end -->

@endsection