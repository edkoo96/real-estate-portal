@extends('public.layouts.app')

@section('title', 'index')

@section('content')

    <!-- Hero section -->
    <section class="hero-section set-bg" data-setbg="assets_public/img/bg.jpg">
        <div class="container hero-text text-white">
            <h2>Hľadáte si vysnívané bývanie?</h2>
            <p>Radi Vám s výberom pomôžeme. <br>Všetky informácie o nehnuteľnostiach na jednom mieste.</p>
        </div>
    </section>
    <!-- Hero section end -->

    <!-- Filter form section -->
    <div class="filter-search">
        <div class="container">
            <form action="{{URL::to('search/')}}" method="POST" class="filter-form" role="search">
                {{ csrf_field() }}
                <input type="text" placeholder="Zadajte hľadaný výraz" name="keyword" id="keyword" >
                <select id="type" name="type">
                    <option value="0">hladať všetky typy</option>
                    @foreach($estate_types as $estate_type)
                        <option value="{{$estate_type->id}}">{{$estate_type->name}}</option>
                    @endforeach
                </select>
                <button class="site-btn fs-submit">Hľadať</button>
                <p></p>
            </form>
        </div>
    </div>
    <!-- Filter form section end -->

    <!-- Properties section -->
    <section class="properties-section spad">
        <div class="container">
            <div class="section-title text-center">
                <h3>Najnovšie ponuky</h3>
                <p>Objavte tie najnovšie inzeráty z našej ponuky</p>
            </div>
            <div class="row">
                @forelse($data_details as $estate_detail)
                    <div class="col-md-6">
                        <div class="propertie-item set-bg" data-setbg="{{url($estate_detail->photos->first()['photo_path'])}}">
                            <div class="{{$estate_detail->estate_type_offer}}-notic">{{$estate_detail->estate_type_offer}}</div>
                            <div class="propertie-info text-white">
                                <div class="info-warp">
                                    <h5>{{$estate_detail->name}}</h5>
                                    <p><i class="fa fa-map-marker"></i> {{$estate_detail->address}}</p>
                                </div>
                                <a href="{{url('single_list/'. $estate_detail->id)}}" class="price">{{$estate_detail->prize}} € {{ $estate_detail->estate_type_offer == 'rent' ? ' /mesiac' : '' }}</a>
                            </div>
                        </div>
                    </div>
                @empty
                    <p><img src="{{url('assets_public/img/empty.jpg')}}"></p>
                @endforelse
            </div>
        </div>
    </section>
    <!-- Properties section end -->

    <!-- feature category section -->
    <section class="feature-category-section spad set-bg" data-setbg="assets_public/img/gallery/0.jpg">
        <div class="container">
            <div class="section-title text-center">
                <h3>AKÉ BÝVANIE HĽADÁTE?</h3>
                <p>Pomôžeme Vám s výberom vysnívanej nehnuteľnosti</p>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 f-cata">
                    <div class="primary">
                        <a href="{{url('categories/domy')}}"><img src="assets_public/img/feature-cate/2.jpg" alt=""></a></div>
                    <h5>Domy</h5>
                </div>
                <div class="col-lg-3 col-md-6 f-cata">
                    <div class="primary">
                        <a href="{{url('categories/byty')}}"><img src="assets_public/img/feature-cate/1.jpg" alt=""></a></div>
                    <h5>Byty</h5>
                </div>
                <div class="col-lg-3 col-md-6 f-cata">
                    <div class="primary">
                        <a href="{{url('categories/priestory')}}"><img src="assets_public/img/feature-cate/3.jpg" alt=""></a></div>
                    <h5>Priestory</h5>
                </div>
                <div class="col-lg-3 col-md-6 f-cata">
                    <div class="primary">
                        <a href="{{url('categories/pozemky')}}"><img src="assets_public/img/feature-cate/4.jpg" alt=""></a></div>
                    <h5>Pozemky</h5>
                </div>
            </div>
        </div>
    </section>
    <!-- feature category section end-->

    <!-- Gallery section -->
    <section class="gallery-section spad">
        <div class="container">
            <div class="section-title text-center">
                <h3>Populárne regióny</h3>
                <p>Vyhľadávajte ponuky podľa regiónov</p>
            </div>
            <div class="gallery">
                <div class="grid-sizer"></div>
                <a href="{{url('cities/poprad')}}" class="gallery-item grid-long set-bg" data-setbg="assets_public/img/gallery/1.jpg">
                    <div class="gi-info">
                        <h3>Poprad</h3>
                        <p>Počet ponúk: {{$poc_poprad}}</p>
                    </div>
                </a>
                <a href="{{url('cities/nitra')}}" class="gallery-item grid-wide set-bg" data-setbg="assets_public/img/gallery/2.jpg">
                    <div class="gi-info">
                        <h3>Nitra</h3>
                        <p>Počet ponúk: {{$poc_nitra}}</p>
                    </div>
                </a>
                <a href="{{url('cities/krupina')}}" class="gallery-item set-bg" data-setbg="assets_public/img/gallery/3.jpg">
                    <div class="gi-info">
                        <h3>Krupina</h3>
                        <p>Počet ponúk: {{$poc_krupina}}</p>
                    </div>
                </a>
                <a href="{{url('cities/bratislava')}}" class="gallery-item set-bg" data-setbg="assets_public/img/gallery/4.jpg">
                    <div class="gi-info">
                        <h3>Bratislava</h3>
                        <p>Počet ponúk: {{$poc_bratislava}}</p>
                    </div>
                </a>
            </div>
        </div>
    </section>
    <!-- Gallery section end -->

    <!-- Services section -->
    <section class="services-section spad set-bg" data-setbg="assets_public/img/service-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <img src="assets_public/img/service.jpg" alt="">
                </div>
                <div class="col-lg-5 offset-lg-1 pl-lg-0">
                    <div class="section-title text-white">
                        <h3>NAŠE SLUŽBY</h3>
                    </div>
                    <div class="services">
                        <div class="service-item">
                            <i class="fa fa-comments"></i>
                            <div class="service-text">
                                <h5>Poradenstvo</h5>
                                <p>Máte predstavu o svojom bývaní? Radi Vám pomôžeme s výberom Vašeho nového domova.</p>
                            </div>
                        </div>
                        <div class="service-item">
                            <i class="fa fa-briefcase"></i>
                            <div class="service-text">
                                <h5>Prenájom a predaj</h5>
                                <p>Umožníme Vám publikovať svoje vlastné inzeráty a následne nájsť záujemcov.</p>
                            </div>
                        </div>
                        <div class="service-item">
                            <i class="fa fa-home"></i>
                            <div class="service-text">
                                <h5>Manažment</h5>
                                <p>O všetko sa postaráme a Vy môžete ostať v pohodlí domova.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Services section end -->


    <!-- Blog section -->
    <section class="blog-section spad">
        <div class="container">
            <div class="section-title text-center">
                <h3>NOVINKY NA BLOGU</h3>
                <p>Správy Real Estate Laboratory - novinky zo sveta nehnuteľností.</p>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 blog-item">
                    <img src="assets_public/img/blog/1.jpg" alt="">
                    <h5><a href="single-blog.html">Interview: Tajomstvá dokonalého interiéru</a></h5>
                    <div class="blog-meta">
                        <span><i class="fa fa-user"></i>Libby Stephen</span>
                        <span><i class="fa fa-clock-o"></i>11 Okt 2018</span>
                    </div>
                    <p>Čo si prvé všimnete, keď vkročíte po prvý raz do reštaurácie, kancelárie alebo do niekoho príbytku? Kreatívny dotyk. Priestory zariadené ...</p>
                </div>
                <div class="col-lg-4 col-md-6 blog-item">
                    <img src="assets_public/img/blog/2.jpg" alt="">
                    <h5><a href="single-blog.html">Ekologické drevené domy za výhodné ceny</a></h5>
                    <div class="blog-meta">
                        <span><i class="fa fa-user"></i>Esther K.</span>
                        <span><i class="fa fa-clock-o"></i>4 Okt 2018</span>
                    </div>
                    <p>Sú nízkoenergetické, nesmierne zdravé a na to, že sa hodia iba na víkendovú chatu, radšej rýchlo zabudnite! Obklopte sa drevom a zažite ten ...</p>
                </div>
                <div class="col-lg-4 col-md-6 blog-item">
                    <img src="assets_public/img/blog/3.jpg" alt="">
                    <h5><a href="single-blog.html">Renovácia starého domu</a></h5>
                    <div class="blog-meta">
                        <span><i class="fa fa-user"></i>L. L.</span>
                        <span><i class="fa fa-clock-o"></i>15 Sep 2018</span>
                    </div>
                    <p>Niektorí ľudia snívajú o precestovaní celého sveta, iní o hviezdnej kariére a napokon sú tu tí, ktorí túžia vytvoriť nádherný domov pre svoju ...</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog section end -->

@endsection