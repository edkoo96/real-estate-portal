@extends('public.layouts.app2')

@section('title', 'O Nás')

@section('content')

    <!-- Breadcrumb -->
    <div class="site-breadcrumb">
        <div class="container">
            <a href="index"><i class="fa fa-home"></i>Domov</a>
            <span><i class="fa fa-angle-right"></i>@yield('title')</span>
        </div>
    </div>
    <!-- Breadcrumb end -->


    <!-- page -->
    <section class="page-section">
        <div class="container">
            <img class="mb-5" src="assets_public/img/about.jpg" alt="">
            <div class="row about-text">
                <div class="col-xl-6 about-text-left">
                    <h5>O NÁS</h5>
                    <p>Na stránkach Real Estate Laboratory Vám ponúkame všetky realitné inzercie pre bežných užívateľov a realitné kancelárie na jednom mieste. Už viac nemusíte navštevovať rôzne portály, my Vám ponúkame všetko ako na dlani. V prípade akýchkoľvek otázok nás neváhajte kontaktovať. Radi vám odpovieme a poradíme.</p>
                </div>
                <div class="col-xl-6 about-text-right">
                    <h5>NAŠE KVALITY</h5>
                    <ul class="about-list">
                        <li><i class="fa fa-check-circle-o"></i>Všetky ponuky nehnuteľností na jednom mieste.</li>
                        <li><i class="fa fa-check-circle-o"></i>Prehľadné a jednoduché stránky na orientáciu.</li>
                        <li><i class="fa fa-check-circle-o"></i>V prípade otázok sme tu vždy pre Vás pripravení pomôcť!</li>
                    </ul>
                </div>
            </div>
        </div>
    <!-- page end-->


    <!-- Team section -->
    <section class="team-section spad pb-0">
        <div class="container">
            <div class="section-title text-center">
                <h3>NAŠE REALITNÉ KANCELÁRIE</h3>
                <p>Zoznam inzerujúcich realitných kancelárií</p>
            </div>
            <div class="row">
                @foreach($data_offices as $office)
                <div class="col-lg-4 col-md-6">
                    <div class="team-member">
                        <div class="member-pic">
                            <img src="{{$office->logo_path}}" alt="#">
                        </div>
                        <div class="member-info">
                            <h5><a href="{{url('profile/' . $office->name)}}">{{$office->name}}</a></h5>
                            <span>{{$office->description}}</span>
                            <div class="member-contact">
                                <p><i class="fa fa-globe"></i>{{$office->url}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        </section>
    </section>
    <!-- Team section end -->

@endsection