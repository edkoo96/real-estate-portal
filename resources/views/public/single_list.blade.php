@extends('public.layouts.app2')

@section('title', 'Inzerát')

@section('content')

    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&libraries=places" type="text/javascript"></script>
    <style>

        #maps-canvas{
            width: 100%;
           height: 300px;
        }

    </style>

    <!-- Breadcrumb -->
    <div class="site-breadcrumb">
        <div class="container">
            <a href="index"><i class="fa fa-home"></i>Domov</a>
            <span><i class="fa fa-angle-right"></i>@yield ('title') </span>
        </div>
    </div>
    <!-- Breadcrumb end -->


    <!-- Page -->
    <section class="page-section">
        <div class="container">
            <div class="row">
                @forelse($data_details as $estate_detail)
                <div class="col-lg-8 single-list-page">
                    <h3>{{$estate_detail->estate_name}}</h3>
                    <div class="single-list-slider owl-carousel" id="sl-slider">
                        @forelse($estate_detail->photos as $row)
                            <div class="sl-item set-bg" data-setbg="{{url($row->photo_path)}}">
                                <div class="{{$estate_detail->estate_type_offer}}-notic">{{$estate_detail->estate_type_offer}}</div>
                            </div>
                            @empty
                            @endforelse
                    </div>
                    <div class="owl-carousel sl-thumb-slider" id="sl-slider-thumb">
                        @forelse($estate_detail->photos as $row)
                        <div class="sl-thumb set-bg" data-setbg="{{url($row->photo_path)}}"></div>
                        @empty
                        @endforelse
                    </div>
                    <div class="single-list-content">
                        <div class="row">
                            <div class="col-xl-8 sl-title">
                                <p><i class="fa fa-map-marker"></i>{{$estate_detail->address}}</p>
                                <p><i class="fa fa-map-o"></i>lat: {{$estate_detail->lat}}, lng: {{$estate_detail->lng}}</p>
                            </div>
                            <div class="col-xl-4">
                                <a href="#" class="price-btn">{{$estate_detail->prize}} € {{ $estate_detail->estate_type_offer == 'rent' ? ' /mesiac' : '' }}</a>
                            </div>
                        </div>
                        <h3 class="sl-sp-title">Detaily nehnuteľnosti</h3>
                        <div class="row property-details-list">
                            <div class="col-md-4 col-sm-6">
                                <p><i class="fa fa-th-large"></i> Rozloha: {{$estate_detail->floor_space}} m²</p>
                                <p><i class="fa fa-user"></i>{{$estate_detail->clients_contact->name}} {{$estate_detail->clients_contact->surname}}</p>
                                <p><i class="fa fa-phone"></i>{{$estate_detail->clients_contact->telephone}}</p>
                                <p><i class="fa fa-envelope"></i>{{$estate_detail->clients_contact->email}}</p>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <p><i class="fa fa-bed"></i>Počet izieb: {{$estate_detail->number_of_bedrooms}}</p>
                                <p><i class="fa fa-shower"></i>Počet kúpelní: {{$estate_detail->number_of_bathrooms}}</p>
                                <p><i class="fa fa-lock"></i>Počet garáží: {{$estate_detail->number_of_garages}}</p>
                                <p><i class="fa fa-car"></i>Miesta na parkovanie: {{$estate_detail->number_of_parking_space}}</p>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <p><i class="fa fa-clock-o"></i>Inzerát bol pridaný: {{date('d.m.y', strtotime($estate_detail->created_at))}}</p>
                            </div>
                        </div>
                        <h3 class="sl-sp-title">Popis</h3>
                        <div class="description">
                            <p>{!! $estate_detail->description !!}</p>
                        </div>
                        <h3 class="sl-sp-title bd-no">Mapa</h3>
                        <div class="pos-maps" id="maps-canvas"></div>
                    </div>
                </div>
                    <script>

                        var map = new google.maps.Map(document.getElementById('maps-canvas'),{
                            center:{
                                lat: <?php echo $estate_detail->lat; ?>,
                                lng: <?php echo $estate_detail->lng; ?>
                            },
                            zoom:15
                        })

                        var marker = new google.maps.Marker({
                            position: {
                                lat: <?php echo $estate_detail->lat; ?>,
                                lng: <?php echo $estate_detail->lng; ?>
                            },
                            map: map,
                            draggable: false
                        });

                    </script>
                @empty
                @endforelse

                @forelse($data_agents as $agent)

                <div class="col-lg-4 col-md-7 sidebar">
                    <div class="author-card">
                        <div class="author-img set-bg"><img src="{{ url( $agent->avatar_path ) }}" width="100" class="img-thumbnail" alt="Avatar"></div>
                        <div class="author-info">
                            <h5> <a href="{{url('profile/agents/' . $agent->id)}}">{{$agent->name}} {{$agent->surname}}</a> </h5>
                            <a href="{{url('profile/' . $agent->reality_offices->name)}}">{{$agent->reality_offices->name}}</a>
                        </div>
                        <div class="author-contact">
                            <p><i class="fa fa-phone"></i>{{$agent->telephone}}</p>
                            <p><i class="fa fa-envelope"></i>{{$agent->email}}</p>
                        </div>
                    </div>
                @empty
                @endforelse
                    <div class="related-properties">
                        <h2>Ďalšie ponuky od makléra</h2>
                        @forelse($data_others as $others)
                        <div class="rp-item">
                            <div class="rp-pic set-bg" data-setbg="{{url($others->photos->first()['photo_path'])}}">
                                <div class="{{$others['estate_type_offer']}}-notic">{{$others['estate_type_offer']}}</div>
                            </div>
                            <div class="rp-info">
                                <h5>{{$others['estate_name']}}</h5>
                                <p><i class="fa fa-map-marker"></i>{{$others['address']}}</p>
                            </div>
                            <a href="{{url('single_list/'. $others->id)}}" class="rp-price">{{$others['prize']}} € {{ $others['estate_type_offer'] == 'rent' ? ' /mesiac' : '' }}</a>
                        </div>
                        @empty
                            <p><img src="{{url('assets_public/img/empty.jpg')}}"></p>
                        @endforelse
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- Page end -->

@endsection