@extends('public.layouts.app2')

@section('title', 'Profil')

@section('content')

    <!-- Breadcrumb -->
    <div class="site-breadcrumb">
        <div class="container">
            <a href="index"><i class="fa fa-home"></i>Domov</a>
            <span><i class="fa fa-angle-right"></i>@yield('title')</span>
        </div>
    </div>
    <!-- Breadcrumb end -->


    <!-- page -->
    <section class="page-section">
        <div class="container">
            @foreach($data_office as $office)
            <div class="row about-text">
                <div class="col-xl-6 about-text-left">
                    <h5>{{$office->name}}</h5>
                    <p>{{$office->description}}</p>
                </div>
                <div class="col-xl-6 about-text-right">
                    <ul class="about-list">
                        <img class="mb-5" src="{{ url( $office->logo_path)}}" height="200" alt="">
                    </ul>
                </div>
            </div>
            @endforeach
        </div>
    <!-- page end-->


    <!-- Team section -->
    <section class="team-section spad pb-0">
        <div class="container">
            <div class="section-title text-center">
                <h3>NAŠI AGENTI</h3>
                <p>Naši agenti sú tu pre Vás</p>
            </div>
            <div class="row">
                @forelse($data_agents as $data_agent)
                <div class="col-lg-4 col-md-6">
                    <div class="team-member">
                        <div class="member-pic">
                            <img src="{{ url( $data_agent->avatar_path)}}" alt="#">
                        </div>
                        <div class="member-info">
                            <h5><a href="{{url('profile/agents/' . $data_agent->id)}}">{{$data_agent->name}} {{$data_agent->surname}}</a></h5>
                            <span>{{$data_agent->reality_offices->name}} Agent</span>
                            <div class="member-contact">
                                <p><i class="fa fa-phone"></i>{{$data_agent->telephone}}</p>
                                <p><i class="fa fa-envelope"></i>{{$data_agent->email}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                    <p><img src="{{url('assets_public/img/empty.jpg')}}"></p>
                @endforelse
            </div>
        </div>
        </section>
    </section>
    <!-- Team section end -->

@endsection