@extends('public.layouts.app2')

@section('title', 'Naša ponuka')

@section('content')

    <div class="container">
        <p> <br><br> </p>
    </div>

    <!-- Breadcrumb -->
    <div class="site-breadcrumb">
        <div class="container">
            <a href="index"><i class="fa fa-home"></i>Domov</a>
            <span><i class="fa fa-angle-right"></i>@yield('title')</span>
        </div>
    </div>
    <!-- Breadcrumb end -->

    <!-- page -->
    <section class="page-section categories-page">
        <div class="container"
        >
            <div class="filter-search">
                <div class="container">
                    <tr>
                        <th> Zoradiť podla: </th>
                        <th> @sortablelink('estate_name', 'Názvu') </th>
                        <th> @sortablelink('prize', 'Ceny') </th>
                        <th> @sortablelink('floor_space', 'Rozlohy') </th>
                        <th> @sortablelink('created_at', 'Dátumu pridania') </th>
                    </tr>
                </div>
            </div>

            <div class="row">
                @forelse($data_details as $estate_detail)
                <!-- feature -->
                <div class="col-lg-4 col-md-6">
                    <div class="feature-item">
                        <div class="feature-pic set-bg" data-setbg="{{url($estate_detail->photos->first()['photo_path'])}}">
                            <div>
                                <div class="{{$estate_detail->estate_type_offer}}-notic">{{$estate_detail->estate_type_offer}}</div>
                            </div>
                        </div>
                        <div class="feature-text">
                            <div class="text-center feature-title">
                                <h3>{{$estate_detail->estate_name}}</h3>
                                <p><i class="fa fa-map-marker"></i> {{$estate_detail->address}}</p>
                            </div>
                            <div class="room-info-warp">
                                <div class="room-info">
                                    <div class="rf-left">
                                        <p><i class="fa fa-user">{{$estate_detail->clients_contact->name}} {{$estate_detail->clients_contact->surname}}</i></p>
                                    </div>
                                    <div class="rf-right">
                                        <p><i class="fa fa-clock-o">{{date('d.m', strtotime($estate_detail->created_at))}}</i></p>
                                    </div>
                                </div>
                            </div>
                            <a href="{{url('single_list/'. $estate_detail->id)}}" class="room-price">{{$estate_detail->prize}} €</a>
                        </div>
                    </div>
                </div>
                @empty
                    <p><img src="{{url('assets_public/img/empty.jpg')}}"></p>
                 @endforelse
            </div>
        </div>
    </section>
    <!-- page end -->

@endsection