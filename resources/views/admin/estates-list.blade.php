@extends('admin.layouts.app')

@section('title', 'Zoznam nehnuteľností')

@section('content')

    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <h3 class="page-title">Zoznam nehnuteľností</h3>
                <div class="panel panel-headline">
                    <div class="panel-body">

                        <table id="example" class="table table-striped" style="width:100%">
                            <thead>
                            <tr>
                                <th>Nazov</th>
                                <th>Mesto/Obec</th>

                            </tr>
                            </thead>
                            <tbody>
                            @forelse($estate_details as $estate_detail)

                            <tr>
                                <td><a href="{{action("EstateController@edit",['id'=>$estate_detail->id])}}"> {{$estate_detail->estate_name}}</a></td>
                                <td>{{$estate_detail->address}}
                                    <div class="text-center">
                                        <a href="{{action("EstateController@delete",['id'=>$estate_detail->id])}}" class="btn btn-primary">Vymaz nehnutelnost</a>
                                    </div>

                                </td>
                            </tr >

                            @empty
                                <p>nie je nic</p>
                            @endforelse
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Nazov</th>
                                <th>Mesto/Obec</th>
                            </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->

@endsection