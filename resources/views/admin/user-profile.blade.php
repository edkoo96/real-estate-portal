@extends('admin.layouts.app')

@section('title', 'Profil makléra')

@section('content')

    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel panel-profile">
                    <div class="clearfix">
                        <!-- LEFT COLUMN -->
                        <div class="profile-left">
                            <!-- PROFILE HEADER -->
                            <div class="profile-header">
                                <div class="overlay"></div>
                                <div class="profile-main">
                                    <img src="{{ url($user->avatar_path) }}" class="img-circle" alt="Avatar" width="150">
                                    <h3 class="name">{{$user->name . ' ' . $user->surname}}</h3>
                                </div>

                            </div>
                            <!-- END PROFILE HEADER -->
                            <!-- PROFILE DETAIL -->
                            <div class="profile-detail">
                                <div class="profile-info">
                                    <h4 class="heading">Základné informácie</h4>
                                    <ul class="list-unstyled list-justify">

                                        <li>Meno <span>{{$user->name}}</span></li>
                                        <li>Priezvisko <span>{{$user->surname}}</span></li>
                                        <li>Email <span>{{$user->email}}</span></li>
                                    </ul>
                                </div>


                                <div class="text-center"><a href="{{action("UserController@edit",['id'=>$user->id])}}" class="btn btn-primary">Upraviť profil</a>
                                <a href="{{action("UserController@delete",['id'=>$user->id])}}" class="btn btn-primary">Vymaž používateľa</a>
                                </div>
                            </div>
                            <!-- END PROFILE DETAIL -->
                        </div>
                        <!-- END LEFT COLUMN -->
                        <!-- RIGHT COLUMN -->
                        <div class="profile-right">
                            <h4 class="heading">Pridané nehnuteľnosti</h4>


                            <!-- BASIC TABLE -->
                            <div class="panel" style="min-height: 500px">

                                <div class="panel-body">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Názov</th>
                                            <th>Mesto</th>
                                            <th>Dátum</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($estates as $estate)
                                        <tr>

                                            <td>{{$estate->estate_name}}</td>
                                            <td>{{$estate->address}}</td>
                                            <td>{{$estate->created_at}}</td>

                                        </tr>
                                        @empty
                                            <p>Nenachádza sa tu žiadny záznam.</p>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END BASIC TABLE -->

                        </div>
                        <!-- END RIGHT COLUMN -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->

@endsection