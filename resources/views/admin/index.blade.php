@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')

    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <!-- OVERVIEW -->
                <div class="panel panel-headline">
                    <div class="panel-heading">
                        <h3 class="panel-title">Prehľad realitnej kancelárie</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="metric">
                                    <span class="icon"><i class="fa fa-user"></i></span>
                                    <p>
                                        <span class="title">Počet Maklérov</span>
                                        <span class="number">{{$poc_user}}</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="metric">
                                    <span class="icon"><i class="fa fa-home"></i></span>
                                    <p>
                                        <span class="title">Počet Nehnuteľností</span>
                                        <span class="number">{{$poc_est}}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END OVERVIEW -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- RECENT PURCHASES -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Nehnuteľnosti</h3>
                            </div>
                            <div class="panel-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Id.</th>
                                        <th>Názov</th>
                                        <th>Cena</th>
                                        <th>Dátum pridania</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($data as $user_data)
                                    <tr>
                                        <td>{{$user_data->id}}</td>
                                        <td><a href="{{action("EstateController@edit",['id'=>$user_data->id])}}">{{$user_data->estate_name}}</a></td>
                                        <td>{{$user_data->prize}} €</td>
                                        <td>{{date('d.m.y', strtotime($user_data->created_at))}}</td>
                                        <td><span class="label label-success">{{$user_data->estate_status}}</span></td>
                                    </tr>
                                    @empty

                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Posledné pridané</span></div>
                                </div>
                            </div>
                        </div>
                        <!-- END RECENT PURCHASES -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->

@endsection
