<!-- LEFT SIDEBAR -->

<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li><a href="{{url('admin/')}}" class="{{ Request::is('admin/') ? 'active' : '' }}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
                @if (Auth::user() && Auth::user()->role == 'admin')
                <li>
                    <a href="#subPages" data-toggle="collapse" class="{{ Request::is('admin/users*') ? 'active' : 'collapsed' }}"><i class="lnr lnr-users"></i> <span>Makléri</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="subPages" class="collapse {{ Request::is('admin/users*') ? 'in' : '' }}">
                        <ul class="nav">
                            <li><a href="{{url('admin/users-list')}}" class="{{ Request::is('admin/users-list') ? 'active' : '' }}">Zoznam</a></li>
                            <li><a href="{{url('admin/users-add')}}" class="{{ Request::is('admin/users-add') ? 'active' : '' }}">Pridať</a></li>
                        </ul>
                    </div>
                </li>
                @endif
                <li>
                    <a href="#subPages1" data-toggle="collapse" class="{{ Request::is('admin/estates*') ? 'active' : 'collapsed' }}"><i class="lnr lnr-list"></i> <span>Nehnuteľnosti</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="subPages1" class="collapse {{ Request::is('admin/estates*') ? 'in' : '' }}">
                        <ul class="nav">
                            <li><a href="{{url('admin/estates-list')}}" class="{{ Request::is('admin/estates-list') ? 'active' : '' }}">Zoznam</a></li>
                            <li><a href="{{url('admin/estates-add')}}" class="{{ Request::is('admin/estates-add') ? 'active' : '' }}">Pridať</a></li>
                        </ul>
                    </div>
                </li>
                @if (Auth::user() && Auth::user()->role == 'admin')
                <li><a href="{{url('admin/office-settings')}}"><i class="lnr lnr-cog"></i> <span>Nastavenia</span></a></li>
                @endif
            </ul>
        </nav>
    </div>
</div>

<!-- END LEFT SIDEBAR -->