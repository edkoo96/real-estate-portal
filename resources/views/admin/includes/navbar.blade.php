<!-- NAVBAR -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="brand">
        <a href="/admin/"><img src="{{url('assets_admin/img/logo-dark.png')}}" alt="Klorofil Logo" class="img-responsive logo" width="195px" style="margin-top:-15px;"></a>
    </div>
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
        </div>

        <div id="navbar-menu">
            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <img src="{{ url( Auth::user()->avatar_path ) }}" class="img-circle" alt="Avatar">
                        {{ Auth::user()->name }}
                        <i class="icon-submenu lnr lnr-chevron-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/')}}"><i class="lnr lnr-home"></i>Verejná stránka</a></li>
                        <li><a href="{{url('admin/my-profile')}}"><i class="lnr lnr-user"></i> <span>Môj profil</span></a></li>
                        <li><a href="{{url('admin/edit-profile')}}"><i class="lnr lnr-cog"></i> <span>Nastavenia</span></a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="lnr lnr-exit"></i> <span>Odhlásiť sa</span></a>

                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</nav>
<!-- END NAVBAR -->