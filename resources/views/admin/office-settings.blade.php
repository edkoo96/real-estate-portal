@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')


    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <h3 class="page-title">Nastavenie realitnej kancelárie</h3>

                <div class="row">
                    <div class="col-md-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Všeobecné informácie</h3>
                            </div>
                            <div class="panel-body">
                                <form id="basic-form" method="post" action="{{ action('SettingsController@updateOffice',['id' => $office->id])}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <label class="control-label">Názov</label>
                                        <input type="text" id="name" name="name" class="form-control" value={{$office->name}}>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Popis</label>
                                        <textarea class="form-control" rows="5" cols="30" required="" name="description">{{$office->description}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">URL</label>
                                        <input type="text" id="url" name="url" class="form-control" value={{$office->url}}>
                                    </div>
                                    <br>
                                    <button type="submit" class="btn btn-primary">Uložiť</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Logo kancelárie</h3>
                            </div>
                            <div class="panel-body">
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                                <form action="{{ URL::to('admin/upload-logo') }}" method="post" enctype="multipart/form-data">

                                    <input type="file" name="file" id="file">
                                    <img id="preview" src="{{ URL::to($office->logo_path) }}" alt="" width="90%"/><br><br>
                                    <button type="submit" class="btn btn-primary">Uložiť</button>
                                    <input type="hidden" value="{{ csrf_token() }}" name="_token">

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <script>

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#file").change(function() {
            readURL(this);
        });

    </script>
@endsection
