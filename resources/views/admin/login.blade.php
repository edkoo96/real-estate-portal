@extends('admin.layouts.app-login')


@section('title', 'Login')

@section('content')

    <div class="vertical-align-wrap">
        <div class="vertical-align-middle">
            <div class="auth-box ">
                <div class="left">
                    <div class="content">
                        <div class="header">
                            <div class="logo text-center"><img src="../assets_admin/img/logo-dark.png" alt="Estate LAB" width="300px"></div>
                            <p class="lead">Prihlásiť sa do účtu</p>
                        </div>
                        <form class="form-auth-small" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <label for="signin-email" class="control-label sr-only">Email</label>
                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail">

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="signin-password" class="control-label sr-only">Heslo</label>
                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Heslo">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary btn-lg btn-block">{{ __('Prihlásiť') }}</button>
                            <div class="bottom">
                                <span class="helper-text"><i class="fa fa-lock"></i> <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Zabudli ste heslo?') }}
                                    </a></span>
                            </div>


                        </form>
                    </div>
                </div>
                <div class="right">
                    <div class="overlay"></div>
                    <div class="content text">
                        <h1 class="heading">Portál realitných kancelárií</h1>
                        <p>administrácia</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

@endsection