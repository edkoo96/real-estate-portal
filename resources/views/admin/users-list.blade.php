@extends('admin.layouts.app')

@section('title', 'Zoznam maklérov')

@section('content')

    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <h3 class="page-title">Zoznam maklérov</h3>
                <div class="panel panel-headline">
                    <div class="panel-body">

                        @if (Auth::user() && Auth::user()->role == 'admin')

                        <table id="example" class="table table-striped" style="width:100%">
                            <thead>

                            <tr>
                                <th>Meno</th>
                                <th>Priezvisko</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td><a href="user-profile/{{$user->id}}">{{$user->name}}</a></td>
                                <td>{{$user->surname}}</td>
                                <td>{{$user->email}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Meno</th>
                                <th>Priezvisko</th>
                                <th>Email</th>
                            </tr>
                            </tfoot>
                        </table>

                        @else



                        @endif

                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->

@endsection