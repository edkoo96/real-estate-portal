<!DOCTYPE html>
<html>

<head>
    @include('admin.includes.head')
</head>

<body>

    <div id="wrapper">

        @yield('content')

    </div>

    @include('admin.includes.footer')
    @include('admin.includes.scripts')

</body>
</html>