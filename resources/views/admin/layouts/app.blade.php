<!DOCTYPE html>
<html>

<head>
    @include('admin.includes.head')
</head>

<body>
    @if (Auth::check()==false) redirect('admin/login'); @endif
    <div id="wrapper">

        @include('admin.includes.navbar')
        @include('admin.includes.sidebar')

        @yield('content')

    </div>

    @include('admin.includes.footer')
    @include('admin.includes.scripts')

</body>
</html>