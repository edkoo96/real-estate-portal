@extends('admin.layouts.app')

@section('title', 'Pridať makléra')

@section('content')

    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <h3 class="page-title">Pridať makléra</h3>
                <div class="panel panel-headline">
                    <div class="panel-body">

                        <div class="panel-heading">
                            <h3 class="panel-title">Maklér</h3>
                        </div>
                        <div class="panel-body">

                            <form class="form-auth-small" method="post" onsubmit="return Validate(this)" action="{{ action('UserController@update',['id' => $user->id])}}" >
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <label for="name">Meno:</label>
                                <input type="text" id="name" name="name" class="form-control" value={{$user->name}}><br>
                                <p id="demo"></p>
                                <label for="surname">Priezvisko:</label>
                                <input type="text" id="surname" name="surname" class="form-control" value={{$user->surname}}><br>

                                <label for="email">Email:</label>
                                <input type="text" id="email" name="email" class="form-control" value={{$user->email}}><br>

                                <label for="tel">Telefón:</label>
                                <input type="text" id="tel" name="telephone" class="form-control" value={{$user->telephone}}><br>

                                <label for="role">Rola:</label>
                                <select class="form-control" id="role" name="role" >
                                    <option value="broker">Maklér</option>
                                    <option value="admin">Admin</option>
                                </select><br>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Uložiť</button>
                            </form>
                            <script type="text/javascript">

                               function Validate(form) {

                                   if(form.name.value==''){
                                       alert('Zadajte meno');

                                       form.name.focus();
                                       return false;
                                    }
                                    if(form.surname.value==''){
                                        alert('Zadajte heslo');
                                        //  document.getElementById('demo').innerHTML=txt='tet';
                                        // document.forms['name'].focus();
                                        form.surname.focus();
                                        return false;
                                    }
                                    if(form.email.value==''){
                                        alert('Zadajte heslo');
                                        //  document.getElementById('demo').innerHTML=txt='tet';
                                        // document.forms['name'].focus();
                                        form.email.focus();
                                        return false;
                                    }
                                    return true;

                                }
                                /*function nameVerify() {
                                    if(name.value!=="")
                                    name.style.border='1px solid green';
                                    name_error.innerHTML='';
                                    return true;
                                }*/
                            </script>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->

@endsection