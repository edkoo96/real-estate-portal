@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')


    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <h3 class="page-title">Nastavenie profilu</h3>

                <div class="row">
                    <div class="col-md-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Všeobecné informácie</h3>
                            </div>
                            <div class="panel-body">
                                <form id="basic-form" method="post" action="{{ action('SettingsController@updateProfile',['id' => $user->id])}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <label class="control-label">Meno</label>
                                        <input type="text" id="name" name="name" class="form-control" value={{$user->name}}>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Priezvisko</label>
                                        <input type="text" id="surname" name="surname" class="form-control" value={{$user->surname}}>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="text" id="email" name="email" class="form-control" value={{$user->email}} disabled>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Telefón</label>
                                        <input type="text" id="tel" name="tel" class="form-control" value={{$user->telephone}}>
                                    </div>
                                    <br>
                                    <button type="submit" class="btn btn-primary">Uložiť</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Heslo</h3>
                            </div>
                            <div class="panel-body">
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                                <form method="post" action="{{ action('SettingsController@updatePassword',['id' => $user->id])}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <label class="control-label">Nové heslo</label>
                                        <input type="password" id="password" name="password" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Nové heslo znovu</label>
                                        <input type="password" id="confirm_password" name="confirm_password" class="form-control">
                                    </div>
                                    <span id="message"></span>
                                    <br>
                                    <button type="submit" class="btn btn-primary" id="save" name="save" disabled>Uložiť</button>
                                </form>
                                <br><br>
                                <p>*Heslo musí mať minimálne 6 znakov.</p>

                                <script>

                                    $('#password, #confirm_password').on('keyup', function () {
                                        if ($('#password').val() == $('#confirm_password').val()) {
                                            $('#message').html('Zhoduje sa').css('color', 'green');

                                            var value = document.getElementById('password').value;
                                            if (value.length > 5)  document.getElementById("save").disabled = false;

                                        } else
                                            $('#message').html('Nezhoduje sa').css('color', 'red');
                                    }
                                    );

                                </script>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Avatar</h3>
                            </div>
                            <div class="panel-body">
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                                <form action="{{ URL::to('admin/upload-avatar') }}" method="post" enctype="multipart/form-data">

                                    <input type="file" name="file" id="file">
                                    <img id="preview" src="{{ URL::to($user->avatar_path) }}" alt="" width="90%"/><br><br>
                                    <button type="submit" class="btn btn-primary">Uložiť</button>
                                    <input type="hidden" value="{{ csrf_token() }}" name="_token">

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->
    <script>

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#file").change(function() {
            readURL(this);
        });

    </script>
@endsection
