@extends('admin.layouts.app')

@section('title', 'Pridať nehnuteľnosť')

@section('content')


    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&libraries=places" type="text/javascript"></script>
    <style>

        #map-canvas{
            width: 350px;
            height: 250px;
        }

    </style>

    <!-- MAIN -->
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <h3 class="page-title">Upraviť nehnuteľnosť</h3>

                <div class="row">
                    <div class="col-md-12">
                        <!-- SUBMIT TICKETS -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Nehnuteľnosť</h3>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" method="post" role="form" action="{{ action('EstateController@update',['id'=>$estate->id]) }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <fieldset>
                                        <legend>Základné informácie</legend>
                                        <div class="form-group">
                                            <label for="ticket-name" class="col-sm-3 control-label">Názov</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="name" name="estate_name" value="{{$estate->estate_name}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ticket-message" class="col-sm-3 control-label">Popis</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" id="description" name="description" rows="5" cols="30">{{$estate->description}}</textarea>
                                            </div>
                                            <script>
                                                CKEDITOR.replace( 'description' );
                                            </script>
                                        </div>
                                        <div class="form-group">
                                            <label for="type-offer" class="col-sm-3 control-label">Typ ponuky</label>
                                            <div class="col-sm-9">
                                                <select id="type-offer" name="type-offer" class="form-control">
                                                    <option value="{{$estate->estate_type_offer}}">{{$estate->estate_type_offer}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="type" class="col-sm-3 control-label">Typ</label>
                                            <div class="col-sm-9">
                                                <select id="type" name="type_h_or_x" class="form-control">
                                                    <option value="{{$estate->estate_type['id']}}">{{$estate->estate_type['name']}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="status" class="col-sm-3 control-label">Status ponuky</label>
                                            <div class="col-sm-9">
                                                <select id="status" name="status" class="form-control">
                                                    <option value="free">Voľné</option>
                                                    <option value="reserved">Obsadené</option>
                                                    <option value="block">Blokované</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ticket-name" class="col-sm-3 control-label">Cena (s DPH)</label>
                                            <div class="input-group">
                                                <input class="form-control" name="prize" value="{{$estate->prize}}" type="number">
                                                <span class="input-group-addon">€</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ticket-name" class="col-sm-3 control-label">Mesto</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="searchmap" id="searchmap" value="{{$estate->address}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ticket-name" class="col-sm-3 control-label">Mapa</label>
                                            <div class="col-sm-9">
                                                <div id="map-canvas"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ticket-name" class="col-sm-3 control-label">Lat</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="lat" id="lat" value="{{$estate->lat}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ticket-name" class="col-sm-3 control-label">Lng</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="lng" id="lng" value="{{$estate->lng}}">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend>Kontaktné informácie (majiteľ)</legend>
                                        <div class="form-group">
                                            <label for="contact-name" class="col-sm-3 control-label">Meno</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="contact-name" id="contact-name" value="{{$estate->clients_contact['name']}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="contact-surname" class="col-sm-3 control-label">Priezvisko</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="contact-surname" id="contact-surname" value="{{$estate->clients_contact['surname']}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="contact-email" class="col-sm-3 control-label">Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="contact-email" id="contact-email" value="{{$estate->clients_contact['email']}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="contact-tel" class="col-sm-3 control-label">Telefónne číslo</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="contact-tel" id="contact-tel" value="{{$estate->clients_contact['telephone']}}">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend>Vedľajšie informácie</legend>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="bedrooms" class="col-sm-3 control-label">Počet spální</label>
                                                    <div class="col-sm-9">
                                                        <input type="number" class="form-control" name="bedrooms" id="bedrooms" value="{{$estate->number_of_bedrooms}}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="parking" class="col-sm-3 control-label">Počet parkovacích miest</label>
                                                    <div class="col-sm-9">
                                                        <input type="number" class="form-control" name="parking" id="parking" value="{{$estate->number_of_parking_space}}" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="bathrooms" class="col-sm-3 control-label">Počet kúpeľní</label>
                                                    <div class="col-sm-9">
                                                        <input type="number" class="form-control" name="bathrooms" id="bathrooms" value="{{$estate->number_of_bathrooms}}" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="space" class="col-sm-3 control-label">Rozloha</label>
                                                    <div class="col-sm-9">
                                                        <input type="number" class="form-control" name="space" id="space" value="{{$estate->floor_space}}" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="garages" class="col-sm-3 control-label">Počet garáží</label>
                                                    <div class="col-sm-9">
                                                        <input type="number" class="form-control" name="garages" id="garages" value="{{$estate->number_of_garages}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-9">
                                                <button type="submit" class="btn btn-primary btn-block">Uložiť</button>
                                            </div>
                                        </div>

                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <!-- END SUBMIT TICKETS -->

                    </div>
                </div>

            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN -->

    <script>

        var map = new google.maps.Map(document.getElementById('map-canvas'),{
            center:{
                lat: <?php echo $estate->lat; ?>,
                lng: <?php echo $estate->lng; ?>
            },
            zoom:15
        })

        var marker = new google.maps.Marker({
            position: {
                lat: <?php echo $estate->lat; ?>,
                lng: <?php echo $estate->lng; ?>
            },
            map: map,
            draggable: true
        });

        var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

        google.maps.event.addListener(searchBox, 'places_changed', function () {

            var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, places;

            for(i=0; place=places[i]; i++){
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location);
            }

            map.fitBounds(bounds);
            map.setZoom(15);

        });

        google.maps.event.addListener(marker, 'position_changed', function () {

            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#lat').val(lat);
            $('#lng').val(lng);
        })

    </script>


@endsection