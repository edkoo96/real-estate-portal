<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditedUsersAndRealityTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function __construct()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform() ->registerDoctrineTypeMapping('enum', 'string');
    }
    public function up()
    {
        Schema::table('reality_offices', function (Blueprint $table)
        {
            $table->text('description')->nullable()->change();
            $table->string('gps', 20)->nullable()->change();
            $table->string('url', 50)->nullable()->change();
            $table->string('logo_path', 255)->nullable()->change();
        });

        Schema::table('users', function (Blueprint $table)
        {
            $table->string('remember_token', 100)->nullable()->change();
            $table->dateTime('last_login')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reality_offices');
        Schema::dropIfExists('users');
    }
}
