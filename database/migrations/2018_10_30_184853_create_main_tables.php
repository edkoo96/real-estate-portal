<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country_name', 50);
        });

        Schema::create('city', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('county', 50);
        });

        Schema::create('estate_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
        });

        Schema::create('reality_offices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->text('description');
            $table->string('gps', 20);
            $table->string('url', 50);
            $table->string('logo_path', 255);
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('surname', 50);
            $table->string('email', 50);
            $table->string('password', 255);
            $table->string('telephone', 13);
            $table->string('remember_token', 100);
            $table->enum('role', ['superadmin', 'admin', 'broker']);
            $table->timestamps();
            $table->dateTime('last_login');
            $table->integer('reality_office_id')->unsigned()->index()->nullable();

            $table->foreign('reality_office_id')->references('id')->on('reality_offices');
        });

        Schema::create('clients_contact', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('surname', 50);
            $table->string('email', 50);
            $table->string('telephone', 13);
        });


        Schema::create('estate_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('estate_name', 100);
            $table->integer('floor_space');
            $table->integer('number_of_bedrooms');
            $table->integer('number_of_bathrooms');
            $table->integer('number_of_garages');
            $table->integer('number_of_parking_space');
            $table->boolean('pets_allowed');
            $table->text('description');
            $table->text('gps_coords');
            $table->integer('prize');
            $table->enum('estate_status', ['free', 'reserved', 'block']);
            $table->enum('estate_type_offer', ['sale', 'buy']);
            $table->integer('city_id')->unsigned()->index()->nullable();
            $table->integer('estate_type_id')->unsigned()->index()->nullable();
            $table->integer('added_user_id')->unsigned()->index()->nullable();
            $table->integer('client_contact_id')->unsigned()->index()->nullable();

            $table->foreign('city_id')->references('id')->on('city')->onUpdate('cascade');
            $table->foreign('estate_type_id')->references('id')->on('estate_type')->onUpdate('cascade');
            $table->foreign('added_user_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('client_contact_id')->references('id')->on('clients_contact')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country');
        Schema::dropIfExists('city');
        Schema::dropIfExists('estate_type');
        Schema::dropIfExists('reality_offices');
        Schema::dropIfExists('users');
        Schema::dropIfExists('clients_contat');
        Schema::dropIfExists('estate_details');
    }
}
