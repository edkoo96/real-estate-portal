<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditedEstateDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photos', function (Blueprint $table)
        {
            $table->string('photo_path', 255)->change();

            $table->dropForeign(['photo_category_id']);
            $table->dropColumn('photo_category_id');

            $table->integer('estate_details_id')->unsigned()->index()->nullable();

            $table->foreign('estate_details_id')->references('id')->on('estate_details')->onUpdate('cascade');
        });

        Schema::table('estate_details', function (Blueprint $table)
        {
            $table->dropForeign(['photo_category_id']);
            $table->dropColumn('photo_category_id');
            $table->dropForeign(['city_id']);
            $table->dropColumn('city_id');
            $table->dropColumn('gps_coords');

            $table->string('address', 255)->nullable();
            $table->string('lat', 255)->nullable();
            $table->string('lng', 255)->nullable();
        });

        Schema::dropIfExists('photo_category');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
