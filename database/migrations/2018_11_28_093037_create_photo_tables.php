<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo_category', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 50);
        });

        Schema::create('photos', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 50);
            $table->text('description')->nullable();
            $table->string('photo_path', 50);
            $table->integer('photo_category_id')->unsigned()->index()->nullable();

            $table->foreign('photo_category_id')->references('id')->on('photo_category');
        });

        Schema::table('estate_details', function (Blueprint $table)
        {
            $table->integer('photo_category_id')->unsigned()->index()->nullable();

            $table->foreign('photo_category_id')->references('id')->on('photo_category')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo_category');
        Schema::dropIfExists('photos');
    }
}
