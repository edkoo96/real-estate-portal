<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reality_offices extends Model
{
    protected $table = 'reality_offices';
    protected $fillable = ['name','description','gps','url','logo_path','created_at','updated_at'];

    public function users()
    {
        return $this->hasMany(Users::class,'reality_office_id');
    }

}
