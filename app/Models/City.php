<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'city';
    protected $fillable = ['name','county'];

    public function estate_details()
    {
        return $this->hasMany(Estate_details::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }


}

