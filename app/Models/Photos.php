<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photos extends Model
{
    protected $table= 'photos';
    protected $fillable= ['name','description','photo_path','estate_details_id'];
    public $timestamps = false;

    public function Estate_details()
    {
        return $this->belongsTo(Estate_details::class,'estate_details_id');
    }


}
