<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $fillable = ['name','surname','email','telephone','role','created_at','updated_at','last_login','reality_office_id','avatar_path'];
    protected $hidden = ['password'];

    public function Reality_offices()
    {
        return $this->belongsTo(Reality_offices::class,'reality_office_id');
    }

    public function estate_details()
    {
        return $this->hasMany(Estate_details::class,'added_user_id');
    }

    public function sendPasswordResetNotification($token)
    {
        dd(22);
        // Your your own implementation.
        $this->notify(new ResetPasswordNotification($token));
    }



}
