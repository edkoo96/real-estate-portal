<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo_category extends Model
{
    protected $table= 'photo_category';
    protected $fillable= ['name'];

    public function Estate_details()
    {
        return $this->hasOne(Estate_details::class);
    }

    public function Photos()
    {
        return $this->hasMany(Photos::class,photo_category_id);
    }
}
