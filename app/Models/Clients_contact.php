<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clients_contact extends Model
{
    protected $table = 'clients_contact';
    protected $fillable = ['name','surname','email','telephone'];

    public $timestamps = false;

}
