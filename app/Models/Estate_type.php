<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estate_type extends Model
{
    protected $table = 'estate_type';
    protected $fillable = ['name'];

    public function estate_details()
    {
        return $this->hasMany(Estate_details::class);
    }

    public $timestamps = false;

}
