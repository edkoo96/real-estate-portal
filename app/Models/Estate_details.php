<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Estate_details extends Model
{
    use Sortable;

    protected $table = 'estate_details';
    protected $fillable = ['estate_name','floor_space','number_of_bedrooms','number_of_bathrooms','number_of_garages','number_of_parking_space','pets_allowed','description',
        'prize','estate_status','estate_type_offer','address','lat','lng'];

    public $sortable = ['id','estate_name','prize','floor_space','created_at'];

    public function estate_type()
    {
        return $this->belongsTo(Estate_type::class,'estate_type_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class,'city_id');
    }

    public function users()
    {
        return $this->belongsTo(Users::class,'added_user_id');
    }

    public function clients_contact()
    {
        return $this->belongsTo(Clients_contact::class,'client_contact_id');
    }

    public function photos()
    {
        return $this->hasMany(Photos::class,'estate_details_id');
    }

    public $timestamps = true;


}
