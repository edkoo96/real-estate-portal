<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class KryptoniteFound extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $address_from = 'admin@estatelab.eu';
        $subject = 'Nový profil';
        $name = 'EstateLAB';

        return $this->view('mails.new_user')
            ->from($address_from, $name)
            ->subject($subject)
            ->with([ 'name' => $this->data['name'], 'password' => $this->data['password'], 'office' => $this->data['office'] ]);
    }
}
