<?php

namespace App\Http\Controllers;

//use App\Models\Estate_details;


use App\Models\City;
use App\Models\Clients_contact;
use App\Models\Estate_details;

use App\Models\Estate_type;
use App\Models\Photos;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class EstateController extends Controller
{
    public function showAddForm(){

        $estate_types = Estate_type::all();

        return view('admin.estates-add',['estate_types'=>$estate_types]);
    }

    public function show(){

        //$estate_details = Estate_details::where('added_user_id', '=', Auth::user()->id);

        $user = Users::find(Auth::user()->id);
        $estate_details = $user->estate_details;

        return view('admin.estates-list',['estate_details'=>$estate_details]);
    }


    public function create(Request $request)
    {
        $request->validate([
            'contact-name' => 'required|string|max:255',
            'contact-surname' => 'required|string|max:255',
            'contact-email' => 'required|string|email|max:255',
            'contact-tel' => 'required',

            'estate_name' => 'required|string|max:255',
            'price' => 'required',
            'status' => 'required',
            'type-offer' => 'required',
            'description' => 'required|string',
            'searchmap' => 'required|string|max:255',
            'lat' => 'required',
            'lng' => 'required',
            'type' => 'required',

            'bedrooms' => 'required',
            'bathrooms' => 'required',
            'garages' => 'required',
            'parking' => 'required',
            'space' => 'required',

        ]);

        //Kontakt na majitela
        $client = new Clients_contact();
        $client->name = $request->input('contact-name');
        $client->surname = $request->input('contact-surname');
        $client->email = $request->input('contact-email');
        $client->telephone = $request->input('contact-tel');
        $client->save();
        $client_id = $client->id;

        //Zakladne informacie
        $estate = new Estate_details();
        $estate->estate_name = $request->input('estate_name');
        $estate->prize = $request->input('price');
        $estate->estate_status = $request->input('status');
        $estate->estate_type_offer = $request->input('type-offer');
        $estate->description = $request->input('description');

        $estate->address = $request->input('searchmap');
        $estate->lat = $request->input('lat');
        $estate->lng = $request->input('lng');

        $estate->added_user_id = Auth::user()->id;
        $estate->client_contact_id = $client_id;
        $estate->estate_type_id = $request->input('type');;



       //Vedlajsie informacie
        $estate->number_of_bedrooms = $request->input('bedrooms');
        $estate->number_of_bathrooms = $request->input('bathrooms');
        $estate->number_of_garages = $request->input('garages');
        $estate->number_of_parking_space = $request->input('parking');
        $estate->floor_space = $request->input('space');

        $estate->pets_allowed = false;

        $estate->save();

        $estate_id = $estate->id;

        //Pridavanie fotiek
        if(Input::hasFile('photos'))
        {
            $files = $request->file('photos');

            $i = 0;

            foreach($files as $file){

                $file_name = time() . '-' . $i;
                $path = 'uploads/estates/'.$estate_id.'/';

                //$file = Input::file('photo');
                $file->move($path, $file_name);

                $photos = new Photos();
                $photos->name = $request->input('estate_name');
                $photos->description = $request->input('photo_description');
                $photos->estate_details_id = $estate_id;
                $photos->photo_path = $path . '' . $file_name;
                $photos->save();

                $i++;

            }

        }
        else
        {
            $photos = new Photos();
            $photos->name = 'default_photo';
            $photos->estate_details_id = $estate_id;
            $photos->photo_path = 'estates/default.jpg';
            $photos->save();
        }

        return redirect('admin/estates-list');
    }

    public function edit($id){

        $estate=Estate_details::find($id);

        return view('admin.estates-edit',['estate'=>$estate]);
    }

    public function update($id,Request $request){

        $estate = Estate_details::where('id','=',$id)->first();
        $estate->update([
            'estate_name'=>$request->input('estate_name'),
            'floor_space'=>$request->input('space'),
            'number_of_bedrooms'=>$request->input('bedrooms'),
            'number_of_bathrooms'=>$request->input('bathrooms'),
            'number_of_garages'=>$request->input('garages'),
            'number_of_parking_space'=>$request->input('parking'),
            'description'=>$request->input('description'),
            'prize'=>$request->input('prize'),
            'address' => $request->input('searchmap'),
            'lat'=>$request->input('lat'),
            'lng'=>$request->input('lng'),
            'estate_status'=>$request->input('status'),
            'estate_type_offer'=>$request->input('type-offer')
        ]);

        $client=Clients_contact::where('id','=',$estate->client_contact_id)->first();
        $client->update([
            'name'=>$request->input('contact-name'),
            'surname'=>$request->input('contact-surname'),
            'email'=>$request->input('contact-email'),
            'telephone'=>$request->input('contact-tel')
       ]);

        return redirect()->action('EstateController@show');
    }

    public function delete($id){

        $photos = Photos::where('estate_details_id','=', $id);
        $photos->delete();

        $estate=Estate_details::find($id);
        $estate->delete();

        return redirect('admin/estates-list');
    }

    public function addUserEstate(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'telephone' => 'required',

            'estate_name' => 'required|string|max:255',
            'prize' => 'required',
            'type' => 'required',
            'type_offer' => 'required',
            'description' => 'required|string',
            'searchmap' => 'required|string|max:255',
            'lat' => 'required',
            'lng' => 'required',

            'bedrooms' => 'required',
            'bathrooms' => 'required',
            'garages' => 'required',
            'parking' => 'required',
            'space' => 'required',

        ]);

        $client = new Clients_contact();
        $client->name = $request->input('name');
        $client->surname = $request->input('surname');
        $client->email = $request->input('email');
        $client->telephone = $request->input('telephone');
        $client->save();
        $client_id = $client->id;

        $estate = new Estate_details();
        $estate->estate_name = $request->input('estate_name');
        $estate->prize = $request->input('prize');
        $estate->estate_status = 'free';
        $estate->estate_type_offer = $request->input('type_offer');
        $estate->description = $request->input('description');
        $estate->estate_type_id = $request->input('type');

        $estate->address = $request->input('searchmap');
        $estate->lat = $request->input('lat');
        $estate->lng = $request->input('lng');

       // $userestate->added_user_id = Auth::user()->id;
        $estate->client_contact_id = $client_id;

        //Vedlajsie informacie
        $estate->number_of_bedrooms = $request->input('bedrooms');
        $estate->number_of_bathrooms = $request->input('bathrooms');
        $estate->number_of_garages = $request->input('garages');
        $estate->number_of_parking_space = $request->input('parking');
        $estate->floor_space = $request->input('space');

        $estate->pets_allowed = false;

        $estate->save();
        $estate_id = $estate->id;

        if(Input::hasFile('photos'))
        {
            $files = $request->file('photos');

            $i = 0;

            foreach($files as $file){

                $file_name = time() . '-' . $i;
                $path = 'uploads/estates/'.$estate_id.'/';

                //$file = Input::file('photo');
                $file->move($path, $file_name);

                $photos = new Photos();
                $photos->name = $request->input('estate_name');
                $photos->description = 'Pridaj fotku';
                $photos->estate_details_id = $estate_id;
                $photos->photo_path = $path . '' . $file_name;
                $photos->save();

                $i++;

            }

        }
        else
        {
            $photos = new Photos();
            $photos->name = 'default_photo';
            $photos->estate_details_id = $estate_id;
            $photos->photo_path = 'estates/default.jpg';
            $photos->save();
        }

        return redirect('/');
    }

    public function showAddUserEstateForm(){

        $estate_types = Estate_type::all();

        return view('public.add',['estate_types'=>$estate_types]);
    }

}
