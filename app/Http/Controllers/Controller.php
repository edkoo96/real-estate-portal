<?php

namespace App\Http\Controllers;

use App\Models\Users;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function checkLogin()
    {
        if (!Auth::check()) redirect('admin/login');

        $this->middleware('auth');

    }

    public function isAdmin()
    {
       // $get_user= Users::where('id', "=", Auth::id())->get();

      //  if ($get_user->pluck('role')=='admin')

        $role = Auth::user()->role;

        if($role == 'admin')
        {
            print 'isOK';
        }
        else redirect('/admin/dashboard');

    }
}
