<?php

namespace App\Http\Controllers;

use App\Models\Reality_offices;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class UploadController extends Controller
{
    public function uploadLogo(){

        if(Input::hasFile('file')){

            $file = Input::file('file');
            $file->move('uploads', $file->getClientOriginalName());

            $reality_id = Auth::user()->reality_office_id;
            $office=Reality_offices::where('id','=',$reality_id)->first();

            $office->update(['logo_path' => 'uploads/' . $file->getClientOriginalName()]);


        }

        return redirect('admin/office-settings');

    }

    public function uploadAvatar(){

        if(Input::hasFile('file')){

            $file = Input::file('file');
            $file->move('avatars', Auth::user()->email);

            $user_id = Auth::user()->id;
            $user=Users::where('id','=',$user_id)->first();

            $user->update(['avatar_path' => 'avatars/' . Auth::user()->email]);


        }

        return redirect('admin/edit-profile');

    }


}
