<?php

namespace App\Http\Controllers;

use App\Models\Reality_offices;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    public function showOffice(){

        $office = Reality_offices::find(Auth::user()->reality_office_id);

        return view('admin.office-settings', ['office' => $office]);

    }

    public function updateOffice($id,Request $request){

        $office=Reality_offices::where('id','=',$id)->first();
        $office->update([
            'name'=>$request->input('name'),
            'description'=>$request->input('description'),
            'url'=>$request->input('url')]);

        return redirect()->action('SettingsController@showOffice');
    }

    public function showProfile(){

        $user_id = Auth::user()->id;
        $user=Users::findOrFail($user_id);

        $user_estates = $user->estate_details;

        return view('admin.my-profile',['user'=>$user, 'estates' => $user_estates]);
    }

    public function editProfile(){

        $user_id = Auth::user()->id;
        $user=Users::findOrFail($user_id);

        return view('admin.edit-profile',['user'=>$user]);

    }

    public function updateProfile($id,Request $request){

        $user=Users::where('id','=',$id)->first();
        $user->update([
            'name'=>$request->input('name'),
            'surname'=>$request->input('surname'),
            'tel'=>$request->input('tel')]);

        return redirect()->action('SettingsController@editProfile');
    }

    public function updatePassword($id,Request $request){

        $user=Users::where('id','=',$id)->first();
        $user->update(['password' => password_hash($request->input('password'),PASSWORD_DEFAULT)]);

        return redirect()->action('SettingsController@editProfile');
    }
}
