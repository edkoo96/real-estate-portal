<?php

namespace App\Http\Controllers\Main;

use App\Models\Estate_details;
use App\Models\Estate_type;
use App\Models\Users;
use App\Http\Controllers\Controller;
use Kyslik\ColumnSortable\Sortable;

class CategoriesController extends Controller
{
    public function showCategory()
    {
        $data = Estate_details::sortable()->with('photos','users','clients_contact')->get();

        return view('public.categories',['data_details'=>$data]);

    }

    public function index()
    {
       // $estate_details = Estate_details::sortable()->paginate(10);
        //return view('public.categories')->withUsers($estate_details);
    }

    public function showSingle($id)
    {
        $data = Estate_details::with('photos','clients_contact','estate_type')->get()->where("id", "=",$id);
        $agents = Users::with('reality_offices')->where("id", "=" , $data->pluck('added_user_id'))->get();
        $other_estates = Estate_details::with('users','photos')->where("added_user_id", "=" , $data->pluck('added_user_id'))
                        ->where("id", "!=" , $id)->inRandomOrder()->get();

        return view('public.single_list',['data_details'=>$data, 'data_others'=>$other_estates, 'data_agents'=>$agents]);

    }

    public function selectCategory($type)
    {
        $data = Estate_type::where("name", "=",$type)->get();
        $estates = Estate_details::sortable()->where("estate_type_id","=",$data->pluck('id'))->get();

        return view('public.categories',['data_details'=>$estates]);

    }

    public function showTop()
    {
        $rss_items = $this->getRssFeed(3);

        $data = Estate_details::with('photos')->orderBy('id','desc')->take(4)->get();

        $estate_types = Estate_type::all();
        $estate_details = Estate_details::all();

        $poc_nitra= count(Estate_details::where('address', "LIKE", '%' . "Nitra" . '%')->get());
        $poc_bratislava= count(Estate_details::where('address', "LIKE", '%' . "Bratislava" . '%')->get());
        $poc_poprad= count(Estate_details::where('address', "LIKE", '%' . "Poprad" . '%')->get());
        $poc_krupina= count(Estate_details::where('address', "LIKE", '%' . "Krupina" . '%')->get());

        return view('public.index',['data_details'=>$data, 'estate_types'=>$estate_types, 'estate_details'=>$estate_details,
            'poc_nitra'=>$poc_nitra, 'poc_bratislava'=>$poc_bratislava, 'poc_poprad'=>$poc_poprad, 'poc_krupina'=>$poc_krupina, 'rss_items' => $rss_items]);

    }

    public function showPopular($city)
    {
        $data = Estate_details::where('address', "LIKE" , '%' . $city . '%')->get();

        return view('public.categories',['data_details'=>$data]);
    }

    public function showBlog()
    {
        $rss_feeds =  $this->getRssFeed(20);

        return view('public.blog', ['rss_items' => $rss_feeds]);
    }

    public function getRssFeed($count){

        $xml=("https://byvanie.pravda.sk/rss/xml/");

        $xmlDoc = new \DOMDocument();
        $xmlDoc->load($xml);

        $x=$xmlDoc->getElementsByTagName('item');

        $items = array();

        for ($i=0; $i<$count; $i++) {

            $item_title=$x->item($i)->getElementsByTagName('title')
                ->item(0)->childNodes->item(0)->nodeValue;

            $item_link=$x->item($i)->getElementsByTagName('link')
                ->item(0)->childNodes->item(0)->nodeValue;

            $item_desc=$x->item($i)->getElementsByTagName('description')
                ->item(0)->childNodes->item(0)->nodeValue;

            $item_author=$x->item($i)->getElementsByTagName('author')
                ->item(0)->childNodes->item(0)->nodeValue;

            $item_pubDate=$x->item($i)->getElementsByTagName('pubDate')
                ->item(0)->childNodes->item(0)->nodeValue;

            $item_pubDate = date('d.m.Y', strtotime($item_pubDate));

            $item_img=$x->item($i)->getElementsByTagName('enclosure')->item(0)->getAttribute('url');

            array_push($items, ['title' => $item_title, 'link' => $item_link, 'desc' => $item_desc, 'author' => $item_author, 'pubDate' => $item_pubDate, 'img' => $item_img]);
        }

        return $items;
    }

}
