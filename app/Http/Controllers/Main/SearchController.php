<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Estate_details;


class SearchController extends Controller
{
    public function search(){
        $keyword = Input::get('keyword');
        $type = Input::get('type');

        if ($keyword != "")
        {
            $result = Estate_details::where("address", "LIKE", '%' . $keyword . '%')
                ->orWhere("description", "LIKE", '%' . $keyword . '%')
                ->orWhere("estate_name", "LIKE", '%' . $keyword . '%')
                ->orWhere("estate_type_offer", "LIKE", '%' . $keyword . '%')
                ->orWhere("prize", "LIKE", '%' . $keyword . '%')->get();

            if(count($result) > 0)
            {
                if ($type == 0)
                {
                    return view('public.search_results')->withDetails($result->all())->withQuery($keyword);
                }
                else
                    return view('public.search_results')->withDetails($result->where("estate_type_id", "=", $type))->withQuery($keyword);
            }
        }
        return view('public.search_results')->withMessage("Ľutujeme, nenašli sa žiadne výsledky");
    }
}
