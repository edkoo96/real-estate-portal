<?php

namespace App\Http\Controllers\Main;

use App\Models\Estate_details;
use App\Models\Reality_offices;
use App\Models\Users;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function show($name)
    {
        $name_id = Reality_offices::where('name', '=',$name)->get();
        $agents = Users::where('reality_office_id','=', $name_id->pluck('id'))->get();
        return view('public.profile',['data_agents'=>$agents, 'data_office'=>$name_id]);
    }

    public function showAgent($id)
    {
        $agents = Users::where('id','=', $id)->get();
        $estates = Estate_details::where('added_user_id', '=', $id)->get();
        return view('public.profile_agent',['data_agents'=>$agents, 'data_details'=>$estates]);
    }

    public function showAll()
    {
        $offices= Reality_offices::all();

        return view('public.about',['data_offices'=>$offices]);
    }
}
