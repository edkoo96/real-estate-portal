<?php
/**
 * Created by PhpStorm.
 * User: Dextrid
 * Date: 11/6/2018
 * Time: 5:51 PM
 */

namespace App\Http\Controllers;


use App\Mail\KryptoniteFound;
use App\Models\Estate_details;
use App\Models\Reality_offices;
use App\Models\Users;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;


class UserController extends Controller
{
    public function __construct()
    {
        $this->checkLogin();
    }

    public function show(){

       $office=Reality_offices::findOrFail(Auth::user()->reality_office_id);
       $users=$office->users;
       return view('admin.users-list',['users'=>$users]);

    }


    public function create(Request $request)
    {

        $request->validate([
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users'
        ]);

       $random_password = 'admin';
       $random_password_hash = password_hash($random_password,PASSWORD_DEFAULT);

       $user=new Users();
       $user->name=$request->input('name');
       $user->surname=$request->input('surname');
       $user->email=$request->input('email');
       $user->telephone=$request->input('tel');
       $user->role=$request->input('role');
       $user->last_login= Carbon::now();
       $user->password= $random_password_hash;
       $user->reality_office_id = Auth::user()->reality_office_id;
       $user->remember_token = '';
       $user->avatar_path='avatars/default.png';
       $user->save();

       $office = Reality_offices::findOrFail(Auth::user()->reality_office_id);
       $name_office = $office;

       //Send mail -> bolo vám vytvorené konto, vygenerované heslo + upozornenie že treba zmeniť heslo
       $email = $request->input('email');

       $data = ['name' => $email, 'password' => $random_password, 'office' => $name_office];
       $this->send_email($data, $email);

       return redirect('admin/users-list');

    }

    public function delete($id){

       $user=Users::find($id);
       $user->delete();

       return redirect('admin/users-list');

    }

    public function edit($id){

       $user=User::find($id);

       return view('admin.user-edit',['user'=>$user]);

    }

    public function update($id,Request $request){

       $office=User::where('id','=',$id)->first();
       $office->update([
           'name'=>$request->input('name'),
           'surname'=>$request->input('surname'),
           'telephone'=>$request->input('tel'),
           'email'=>$request->input('email')]);

       return redirect()->action('UserController@show');
    }

    public function showDetail($id){

       $user = Users::find($id);
       $user_estates = $user->estate_details;

       return view('admin.user-profile',['user' => $user, 'estates' => $user_estates]);
    }

    public function send_email($data, $email){

        Mail::to($email)->send(new KryptoniteFound($data));
    }

    public function dashboard()
    {
            $data = Estate_details::where('added_user_id', '=', Auth::id())->orderBy('id','desc')->take(5)->get();

            $user = Users::where('id','=', Auth::id())->get();
            $poc_users = count(Users::where('reality_office_id', '=', $user->pluck('reality_office_id'))->get());

            $users = Users::where('reality_office_id', '=', $user->pluck('reality_office_id'))->get();
            $count = 0;
            foreach ($users as $rows)
            {
                foreach ($rows->estate_details as $row)
                {
                    $count++;
                }
            }

            return view('admin.index',['data'=>$data, 'poc_user'=>$poc_users, 'poc_est'=>$count]);
    }
}
