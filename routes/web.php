<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {return view('public.index');});
Route::get('/about', function () {return view('public.about');});
Route::get('/blog', 'Main\CategoriesController@showBlog');

Route::get('/categories', 'Main\CategoriesController@showCategory')->name('public.categories');
Route::get('/categories/{type}', 'Main\CategoriesController@selectCategory')->name('public.categories');
Route::get('/single_list/{id}', 'Main\CategoriesController@showSingle')->name('public.single_list');

//Route::get('estate_details', ['as' => 'public.categories', 'uses' => 'CategoriesController@index']);
//Route::get('estate_details', 'CategoriesController@index');

Route::get('/', 'Main\CategoriesController@showTop')->name('public.index');
Route::get('/index', 'Main\CategoriesController@showTop')->name('public.index');

Route::get('/cities/{city}', 'Main\CategoriesController@showPopular')->name('public.categories');
Route::get('/profile/{name}', 'Main\ProfileController@show')->name('public.profile');
Route::get('/profile/agents/{id}', 'Main\ProfileController@showAgent')->name('public.profile');
Route::get('/about', 'Main\ProfileController@showAll')->name('public.about');

Route::post('/search','Main\SearchController@search');

Route::get('/contact', function () {return view('public.contact');});
Route::get('/single_blog', function () {return view('public.single_blog');});
Route::get('/add', ['as'=>'add','uses'=>'EstateController@showAddUserEstateForm']);
Route::post('/add',['as'=>'add','uses'=>'EstateController@addUserEstate']);

Route::prefix('admin')->group(function () {

    Route::get('login', function () {
        return view('admin.login');
    });

    Route::get('/', 'UserController@dashboard')->name('admin.index');

    Route::get('dashboard', function () {
        return view('admin.index');
    });
    Route::get('users-list', function () {
        return view('admin.users-list');
    });
    Route::get('users-add', function () {
        return view('admin.users-add');
    });
    Route::get('estates-list', function () {
        return view('admin.estates-list');
    });
    Route::get('estates-add', function () {
        return view('admin.estates-add');
    });


    Route::get('my-profile',
        ['as'=>'my-profile','uses'=>'SettingsController@showProfile']
    );

    Route::get('edit-profile',
        ['as'=>'edit-profile','uses'=>'SettingsController@editProfile']
    );
    Route::post('update-profile/{id}',
        ['as'=>'update-profile','uses'=>'SettingsController@updateProfile']
    );
    Route::post('update-password/{id}',
        ['as'=>'update-password','uses'=>'SettingsController@updatePassword']
    );


    Route::get('office-settings',
        ['as'=>'office-settings','uses'=>'SettingsController@showOffice']
    );
    Route::post('office-update/{id}',
        ['as'=>'office-update','uses'=>'SettingsController@updateOffice']
    );

    Route::post('upload-logo',
        ['as'=>'upload-logo','uses'=>'UploadController@uploadLogo']
    );
    Route::post('upload-avatar',
        ['as'=>'upload-avatar','uses'=>'UploadController@uploadAvatar']
    );


    Route::get('users-list',['as'=>'users-list','uses'=>'UserController@show']);
    Route::post('users-add',['as'=>'users-add','uses'=>'UserController@create']);
    Route::post('user-edit/{id}',['as'=>'user-edit','uses'=>'UserController@update']);
    Route::get('user-profile/{id}',['as'=>'user-profile','uses'=>'UserController@showDetail']);
    Route::get('user-edit/{id}',['as'=>'user-edit','uses'=>'UserController@edit']);
    Route::get('user-delete/{id}',['as'=>'user-delete','uses'=>'UserController@delete']);

    Route::get('estates-list',['as'=>'estates-list','uses'=>'EstateController@show']);
    Route::post('estates-add',['as'=>'estates-add','uses'=>'EstateController@create']);
    Route::get('estates-delete/{id}',['as'=>'estates-delete','uses'=>'EstateController@delete']);
    Route::post('estates-edit/{id}',['as'=>'estates-edit','uses'=>'EstateController@update']);
    Route::get('estates-edit/{id}',['as'=>'estates-edit','uses'=>'EstateController@edit']);

    Route::get('estates-add',['as'=>'estates-add','uses'=>'EstateController@showAddForm']);
    //Route::get('estates-edit/{id}',['as'=>'estates-edit','uses'=>'EstateController@editForm']);
});
// Authentication Routes...

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


// Registration Routes...

Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

//Auth::routes();

//po registrácii
Route::get('/admin/dashboard/', 'UserController@show')->name('admin');

