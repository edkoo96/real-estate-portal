function initialize() {
	var myOptions = {
		zoom: 16,
		center: new google.maps.LatLng(48.151457, 17.876109), //change the coordinates
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: true,
		mapTypeControl: true,
		zoomControl: true,
		streetViewControl: true,
		styles: [{"elementType":"geometry","stylers":[{"color":"#f5f5f5"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f5f5"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#dadada"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#c9c9c9"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]}]
		}

	var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
    var sala = {lat: 48.151457, lng: 17.876109};



    var contentString = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h3 id="firstHeading" class="firstHeading">Kostol sv. Margity Antiochijskej</h3>'+
        '<div id="bodyContent">'+
        '<p>Klasicistická stavba postavená v rokoch 1828 – 1837 na mieste staršieho kostola, ktorý bol vybudovaný najneskôr ' +
		'v 16. storočí. Staviteľom dnešného kostola bol Juraj Schwartz z Trnavy. Stavba je jednoloďová s rovným uzáverom' +
		' presbytéria, zaklenutým kupolou. Loď je zaklenutá pruskou klenbou s medziklenbovými pásmi, ktoré dosahujú na ' +
		'vtiahnuté pilastre s rímsovými hlavicami. Priečelie so vstupným portálom má štyri oblé stĺpy, na ktorých spočíva ' +
		'tympanon. Veža kostola je situovaná na strednú os priečelia a je zakončená strechou v tvare prilby. Na severnej ' +
		'strane je pristavená sakristia. Dva pôvodné bočné oltáre, klasicistické, z čias stavby kostola. majú portikové ' +
		'oltárne usporiadanie. Uprostred portikálnej oltárnej architektúry sú obrazy Zvestovania Panny Márie a sv. ' +
		'Jozefa z konca 19. storočia.</p>'+
        '</div>'+
        '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    var marker = new google.maps.Marker({
        position: sala,
        map: map,

    });
    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });

}
google.maps.event.addDomListener(window, 'load', initialize);